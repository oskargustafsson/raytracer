#include "UnorderedSpace.h"
#include "Vector3Functions.h"

namespace rt {

	bool UnorderedSpace::hit(
		HitRecord<real>& hitRecord,
		Ray<real>& ray,
		const real tMin,
		const real tMax,
		const SceneObject* ignore) const
	{
		bool hasHitSomething = false;

		ray.position -= localPosition;

		Vector3r origRayPos(ray.position);
		real locTMin = tMin;

		for (const auto sceneObject : objects) {
			if (sceneObject != ignore && sceneObject->hit(hitRecord, ray, locTMin, hasHitSomething ? 0 : INFINITY)) {
				locTMin = -(origRayPos - ray.position).length();
				hasHitSomething = true;
			}
		}

		ray.position += localPosition;

		return hasHitSomething;
	}

}