#pragma once

#include <vector>
#include "Scene.h"
#include "SceneObject.h"

namespace rt {

	class Game {

	public:
		Game(Scene& scene);
		void update(real dt);

	private:
		Scene& scene;
		std::vector<SceneObject*> liveObjects;

	};

}

