#pragma once

typedef float real;

const unsigned int WIND_WIDTH = 512;
const unsigned int WIND_HEIGHT = 512;
const unsigned int MAX_FPS = 60;