#pragma once

#include "SceneObject.h"

namespace rt {

	class AAPlane : public SceneObject {

	public:
		AAPlane(
			const SceneObject* parent,
			const Vector3r& position,
			Vector3r::Axis normalAxis,
			const Material& material) :
			SceneObject(parent, position, material),
			normalAxis(normalAxis),
			surfaceOnb(Vector3r(), Vector3r(), Vector3r())
		{
			normal.el[normalAxis] = 1;
			surfaceOnb.w = normal;
			surfaceOnb.u.el[(normalAxis + 1) % 3] = 1;
			surfaceOnb.v.el[(normalAxis + 2) % 3] = 1;
		}

		bool hit(
			HitRecord<real>& hitRecord,
			Ray<real>& ray,
			const real tMin,
			const real tMax,
			const SceneObject* ignore = nullptr) const;

		Vector3r::Axis normalAxis;
		Vector3r normal;
		Basis3r surfaceOnb;

	};

}
