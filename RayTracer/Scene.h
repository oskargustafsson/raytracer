#pragma once

#include "Vector3.h"
#include "Camera.h"
#include "UnorderedSpace.h"

namespace rt {

	class Scene {

	public:
		// TODO: Define default camera elsewhere
		Scene(Camera camera = Camera(nullptr, Vector3r(6.5, 30, 20), Vector3r(6.5, 0, 6.5), Vector3r(0, -1, 0)));

		Camera camera;
		UnorderedSpace root;
	};

}
