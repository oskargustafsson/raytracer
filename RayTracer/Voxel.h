#pragma once

#include "Vector3.h"
#include "UnorderedSpace.h"

namespace rt {

	class Voxel : public UnorderedSpace {

	public:
		Voxel(
			const SceneObject* parent,
			const Material& material) :
			UnorderedSpace(parent, Vector3r(), material) {}

	};

}

