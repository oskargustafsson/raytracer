#include <SFML/Window.hpp>
#include "Camera.h"
#include "Vector3Functions.h"

namespace rt {

	Camera::Camera(SceneObject* parent, Vector3r position, Vector3r lookAt, Vector3r up, real fieldOfView) :
		SceneObject(parent, position),
		basis(Vector3r(), up, lookAt - position),
		lensFactor(tan(fieldOfView / real(2)))
	{
		// Turn it into an orthonormal basis
		basis.w.normalize();
		cross(basis.u, basis.w, basis.v);
		basis.u.normalize();
		cross(basis.v, basis.u, basis.w);
	}

	real const Camera::SPEED = 10;

	void Camera::getLensPoint(Vector3r& out, const Vector2r& position) const {
		out.x = lensFactor * position.x;
		out.y = lensFactor * position.y;
		out.z = 1;
		out *= basis;
	}

	bool Camera::update(real dt) {
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) { localPosition += SPEED * dt * basis.w; }
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) { localPosition -= SPEED * dt * basis.w; }

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) { localPosition -= SPEED * dt * basis.u; }
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) { localPosition += SPEED * dt * basis.u; }

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) { basis.rotate(basis.u, -dt); }
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) { basis.rotate(basis.u, dt); }

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) { basis.rotate(basis.v, dt); }
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) { basis.rotate(basis.v, -dt); }

		return true;
	}

}