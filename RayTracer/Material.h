#pragma once

namespace rt {


	class Material {

	public:
		enum Type { EMIT, DIFF, };

		Material(
			Type type = DIFF,
			const Color& color = Color(),
			const Color& emittance = Color()) :
			type(type),
			color(color),
			emittance(emittance) {}

		Color color;
		Color emittance;
		Type type;

	};

}