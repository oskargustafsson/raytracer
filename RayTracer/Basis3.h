#pragma once

#include "Vector3.h"

namespace rt {

	template <typename T> class Basis3 {

	public:
		Basis3(
			Vector3<T> u = Vector3<T>(),
			Vector3<T> v = Vector3<T>(),
			Vector3<T> w = Vector3<T>()) :
			u(u), v(v), w(w) {}
		Basis3(
			T ux, T vx, T wx,
			T uy, T vy, T wy,
			T uz, T vz, T wz) :
			u(ux, uy, uz),
			v(vx, vy, vz),
			w(wx, wy, wz) {}
		Basis3(const Basis3& b) : u(b.u), v(b.v), w(b.w) {}

		void rotate(const Vector3<T>& vector, const T r) {
			u.rotate(vector, r);
			v.rotate(vector, r);
			w.rotate(vector, r);
		}

		// Members

		Vector3<T> u, v, w;

	};

	typedef Basis3<real> Basis3r;

}