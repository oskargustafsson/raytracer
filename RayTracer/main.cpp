#include <SFML/Graphics.hpp>

#include "Definitions.h"
#include "Scene.h"
#include "Game.h"
#include "RayTracer.h"

auto bgColor = sf::Color::Black;

int main() {
	sf::RenderWindow window(sf::VideoMode(WIND_WIDTH, WIND_HEIGHT), "Ray tracer");
	window.setFramerateLimit(MAX_FPS);

	sf::Clock fpsClock;

	sf::Vector2f fpsBarSize(10, (float)window.getSize().y);
	sf::RectangleShape fpsBar(fpsBarSize);
	fpsBar.setFillColor(sf::Color::Red);
	fpsBar.setPosition(0, 0);
	fpsBar.setOrigin(0, 0);

	sf::Image backBufferImage;
	backBufferImage.create(window.getSize().x, window.getSize().y, bgColor);

	sf::Texture backBufferTexture;
		backBufferTexture.loadFromImage(backBufferImage);

	sf::Sprite backBufferSprite;
	backBufferSprite.setTexture(backBufferTexture, true);
	
	// Game init
	rt::Scene scene;
	rt::Game game(scene);
	rt::RayTracer rayTracer(scene, backBufferImage);

	while (window.isOpen()) {
		sf::Event event;
		while (window.pollEvent(event)) {
			if (event.type == sf::Event::Closed) {
				window.close();
			}
		}

		auto dt = fpsClock.restart().asMicroseconds() / real(1000000);

		game.update(real(dt));

		rayTracer.render();
		backBufferTexture.update(backBufferImage);

		fpsBarSize.y = window.getSize().y * 1 / (dt * real(60));
		fpsBar.setSize(fpsBarSize);

		window.clear();
		window.draw(backBufferSprite);
		window.draw(fpsBar);
		window.display();
	}

	return 0;
}