#pragma once

#include "Definitions.h"

namespace rt {

	template <typename T> union Vector3 {

	public:
		Vector3(T x = 0, T y = 0, T z = 0) : x(x), y(y), z(z) {}
		Vector3(const Vector3& vector) : x(vector.x), y(vector.y), z(vector.z) {}

		void set(const Vector3& v) {
			x = v.x;
			y = v.y;
			z = v.z;
		}

		void set(T ax, T ay, T az) {
			x = ax;
			y = ay;
			z = az;
		}

		T dot(const Vector3& v) const {
			return x * v.x + y * v.y + z * v.z;
		}

		T lengthSq() const {
			return dot(*this);
		}

		T length() const {
			return sqrt(lengthSq());
		}

		void cross(const Vector3& v) {
			auto x = this->x, y = this->y, z = this->z;
			this->x = y * v.z - z * v.y;
			this->y = z * v.x - x * v.z;
			this->z = x * v.y - y * v.x;
		}

		void normalize() {
			auto len = length();
			x /= len;
			y /= len;
			z /= len;
		}

		void add(const Vector3& v) {
			x += v.x;
			y += v.y;
			z += v.z;
		}

		void add(const T& c, const Vector3& v) {
			x += c * v.x;
			y += c * v.y;
			z += c * v.z;
		}

		void sub(const Vector3& v) {
			x -= v.x;
			y -= v.y;
			z -= v.z;
		}

		void mul(const T& c) {
			x *= c;
			y *= c;
			z *= c;
		}

		void mul(const Vector3& v) {
			x *= v.x;
			y *= v.y;
			z *= v.z;
		}

		T maxVal() const {
			return std::max(std::max(x, y), z);
		}

		T maxAbsVal() const {
			return std::max(std::max(abs(x), abs(y)), abs(z));
		}

		T minVal() const {
			return std::mix(std::mix(x, y), z);
		}

		unsigned maxValIndex() const {
			return x > y ? (x > z ? 0 : 2) : (y > z ? 1 : 2);
		}

		unsigned maxAbsValIndex() const {
			auto ax = abs(x), ay = abs(y);
			return ax > ay ? (ax > abs(z) ? 0 : 2) : (ay > abs(z) ? 1 : 2);
		}

		unsigned minValIndex() const {
			return x < y ? (x < z ? 0 : 2) : (y < z ? 1 : 2);
		}

		void rotate(const Vector3& u, const T r) {
			const auto sinr = sin(r), cosr = cos(r), cosr_1m = 1 - cos(r);
			const auto x = u.x, y = u.y, z = u.z;
			const auto xycosr_1m = x * y * cosr_1m, xzcosr_1m = x * z * cosr_1m, yz_cosr_1m = y * z * cosr_1m;
			const auto xsinr = x * sinr, ysinr = y * sinr, zsinr = z * sinr;
			const Basis3<T> t(
				cosr + x * x * cosr_1m, xycosr_1m - zsinr, xzcosr_1m + ysinr,
				xycosr_1m + zsinr, cosr + y * y * cosr_1m, yz_cosr_1m - xsinr,
				xzcosr_1m - ysinr, yz_cosr_1m + xsinr, cosr + z * z * cosr_1m);
			(*this) *= t;
		}

		// Members

		T el[3];
		struct { T x, y, z; };
		struct { T r, g, b; };
		
		enum Axis {
			X = 0,
			Y = 1,
			Z = 2
		};

	};

	// Typedefs

	typedef Vector3<real> Vector3r;
	typedef Vector3<int> Vector3i;
	typedef Vector3<unsigned> Vector3u;
	typedef Vector3r Color;
	typedef Vector3<char> Vector3c;
	typedef Vector3<bool> Vector3b;

	// Points and Vectors are not the same thing (but the difference is only semantic)
	// TODO: make this work
	// template <typename T> using Point3<T> = Vector3<T>;

}