#pragma once

#include <climits>
#include "Definitions.h"

namespace rt {

	class RNG {

	public:
		RNG() {
			reset();
		}

		unsigned long nextULong() {
			x ^= x << 16;
			x ^= x >> 5;
			x ^= x << 1;

			unsigned long t = x;
			x = y;
			y = z;
			z = t ^ x ^ y;

			return z;
		}

		real next() {
			return real(nextULong()) / real(MAX_VAL);
		}

		void reset() {
			x = 123456789;
			y = 362436069;
			z = 521288629;
		}

		static RNG instance;
		const static unsigned long MAX_VAL = ULONG_MAX;

	private:
		unsigned long x, y, z;

	};

}