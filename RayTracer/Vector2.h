#pragma once

#include <SFML/System/Vector2.hpp>
#include "Definitions.h"

namespace rt {

	template <typename T> class Vector2 {

	public:
		Vector2(T x = 0, T y = 0) : x(x), y(y) {}
		Vector2(const Vector2& vector) : x(vector.x), y(vector.y) {}
		Vector2(const sf::Vector2<T>& vector) : x(vector.x), y(vector.y) {}

		T x, y;

	};

	typedef Vector2<real> Vector2r;
	typedef Vector2<unsigned int> Vector2u;

}