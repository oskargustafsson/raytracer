#pragma once

#include "Vector3.h"
#include "Basis3.h"

namespace rt {

	template <typename T> void cross(Vector3<T>& out, const Vector3<T>& a, const Vector3<T>& b) {
		out.x = a.y * b.z - a.z * b.y;
		out.y = a.z * b.x - a.x * b.z;
		out.z = a.x * b.y - a.y * b.x;
	}

	template <typename T> void transform(Vector3<T>& v, const Basis3<T>& b) {
		auto x = v.x, y = v.y, z = v.z;
		v.x = x * b.u.x + y * b.v.x + z * b.w.x;
		v.y = x * b.u.y + y * b.v.y + z * b.w.y;
		v.z = x * b.u.z + y * b.v.z + z * b.w.z;
	}

	// Overload operators (shorthands for the functions above)

	template <typename T> Vector3<T>& operator+=(Vector3<T>& lhs, const Vector3<T>& rhs) {
		lhs.add(rhs);
		return lhs;
	}

	template <typename T> Vector3<T>& operator-=(Vector3<T>& lhs, const Vector3<T>& rhs) {
		lhs.sub(rhs);
		return lhs;
	}

	template <typename T> T operator*(const Vector3<T>& lhs, const Vector3<T>& rhs) {
		return lhs.dot(rhs);
	}

	template <typename T> Vector3<T> operator*(const T c, const Vector3<T>& v) {
		return Vector3<T>(c * v.x, c * v.y, c * v.z);
	}

	template <typename T> Vector3<T>& operator*=(Vector3<T>& lhs, const Basis3<T>& rhs) {
		transform(lhs, rhs);
		return lhs;
	}

	template <typename T> Vector3<T>& operator*=(Vector3<T>& lhs, const T& rhs) {
		lhs.mul(rhs);
		return lhs;
	}

	template <typename T> Vector3<T> operator+(const Vector3<T>& v1, const Vector3<T>& v2) {
		return Vector3<T>(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
	}

	template <typename T> Vector3<T> operator-(const Vector3<T>& v1, const Vector3<T>& v2) {
		return Vector3<T>(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
	}

	template <typename T> Vector3<T> operator*(const Vector3<T>& v, const T c) {
		return Vector3<T>(c * v.x, c * v.y, c * v.z);
	}

	template <typename T> Vector3<T> operator/(const Vector3<T>& v, const T c) {
		return Vector3<T>(v.x / c, v.y / c, v.z / c);
	}

}