#pragma once

#include "Basis3.h"
#include "Vector3.h"
#include "Vector2.h"
#include "SceneObject.h"

namespace rt {

	class Camera : public SceneObject {

	public:
		Camera(SceneObject* parent, Vector3r position, Vector3r lookAt, Vector3r up, real fieldOfView = real(PI) / real(6));

		void getLensPoint(Vector3r& out, const Vector2r& position) const;

		bool update(const real dt);
		bool hit(
			HitRecord<real>& hitRecord,
			Ray<real>& ray,
			const real tMin,
			const real tMax,
			const SceneObject* ignore = nullptr) const 
		{
			return false; 
		}

	private:
		real lensFactor; // TODO: Come up with better name
		Basis3r basis;
		static real const SPEED;
	};

}

