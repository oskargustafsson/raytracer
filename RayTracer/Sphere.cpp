#include "Sphere.h"
#include "Vector3Functions.h"

namespace rt {

	bool Sphere::hit(
		HitRecord<real>& hitRecord,
		Ray<real>& ray,
		const real tMin,
		const real tMax,
		const SceneObject* ignore) const
	{
		auto u = localPosition - ray.position;
		auto a = u * ray.direction;
		auto aSq = a * a;
		auto b = u * u - radiusSq;

		if (b > aSq) { return false; }

		auto tHit = a - sqrt(aSq - b);
		if (tHit < tMin || tMax < tHit) { return false; }

		ray.movePosition(tHit);
		// TODO: implement ONB
		/*hitRecord.normal.add(tHit, ray.direction);
		hitRecord.normal.sub(u);
		hitRecord.normal.normalize();*/
		hitRecord.object = this;

		return true;
	}

	real Sphere::getRadius() const { return radius; }

	void Sphere::setRadius(real aRadius) {
		radius = aRadius;
		radiusSq = radius * radius;
	}

}