#include "Ray.h"

namespace rt {

	void Ray::calculateCachedMembers() {
		invDirection.x = T(1) / direction.x;
		invDirection.y = T(1) / direction.y;
		invDirection.z = T(1) / direction.z;
		directionSigns.x = sign(direction.x);
		directionSigns.y = sign(direction.y);
		directionSigns.z = sign(direction.z);
		directionIsNegative.x = direction.x < 0;
		directionIsNegative.y = direction.y < 0;
		directionIsNegative.z = direction.z < 0;
	}

}