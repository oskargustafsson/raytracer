#include "AAPlane.h"



#include "AAPlane.h"

namespace rt {

	bool AAPlane::hit(
		HitRecord<real>& hitRecord,
		Ray<real>& ray,
		const real tMin,
		const real tMax,
		const SceneObject* ignore) const
	{
		auto tHit = (localPosition.el[normalAxis] - ray.position.el[normalAxis]) * ray.invDirection.el[normalAxis];

		if (tHit < tMin || tMax < tHit) { return false; }

		ray.movePosition(tHit);
		hitRecord.surfaceOnb = surfaceOnb;
		// TODO: handle case where ray hits the back of the plane
		// hitRecord.surfaceOnb.v.el[normalAxis] *= sign(ray.direction.el[normalAxis]);
		hitRecord.object = this;

		return true;
	};

}
