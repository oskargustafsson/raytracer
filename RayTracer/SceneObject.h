#pragma once

#include "Ray.h"
#include "HitRecord.h"
#include "Definitions.h"
#include "Material.h"
#include "Vector3Functions.h"

namespace rt {

	class SceneObject {

	public:
		SceneObject(
			const SceneObject* parent = nullptr,
			const Vector3r& localPosition = Vector3r(),
			const Material& material = Material()) :
			parent(parent),
			localPosition(localPosition),
			material(material) {}

		virtual bool update(const real dt);

		virtual bool hit(
			HitRecord<real>& hitRecord,
			Ray<real>& ray,
			const real tMin,
			const real tMax,
			const SceneObject* ignore = nullptr) const = 0;

		Vector3r getGlobalPosition() const;

		const SceneObject* parent;
		Material material;
		Vector3r localPosition;
	};

}
