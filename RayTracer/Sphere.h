#pragma once

#include "Vector3.h"
#include "SceneObject.h"
#include "Ray.h"
#include "Definitions.h"

namespace rt {

	class Sphere : public SceneObject {

	public:
		Sphere(
			const SceneObject* parent,
			Vector3r position,
			real radius,
			Material material) :
			SceneObject(parent, position, material),
			radius(radius),
			radiusSq(radius * radius) {}

		bool hit(
			HitRecord<real>& hitRecord,
			Ray<real>& ray,
			const real tMin,
			const real tMax,
			const SceneObject* ignore = nullptr) const;

		real getRadius() const;
		void setRadius(real aRadius);

	private:
		real radius;
		real radiusSq;

	};

}