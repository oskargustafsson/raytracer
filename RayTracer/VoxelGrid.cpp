#include "Vector3Functions.h"
#include "VoxelGrid.h"
#include "Math.h"

namespace rt {

	VoxelGrid::VoxelGrid(
		const SceneObject* parent,
		const Vector3u& dims/*,
		real vSize*/) :
		SceneObject(parent),
		dimensions(dims),
		//voxelSize(vSize),
		//invVoxelSize(real(1) / voxelSize),
		//maxPosition(pos.x + dims.x * vSize, pos.y + dims.y * vSize, pos.z + dims.z * vSize),
		toVoxelCenter(real(0.5), real(0.5), real(0.5))
	{
		grid = new Voxel***[dims.x];
		for (unsigned x = 0; x < dims.x; ++x) {
			grid[x] = new Voxel**[dims.y];
			for (unsigned y = 0; y < dims.y; ++y) {
				grid[x][y] = new Voxel*[dims.z];
				for (unsigned z = 0; z < dims.z; ++z) { // initialize the values to whatever you want the default to be
					//grid[x][y][z] = nullptr;
					//grid[x][y][z] = (x + y + z) % 4 == 0 ?
					//	new Voxel(Material(Material::Type::DIFF, Color(real(x) / 16, real(y) / 2, real(z) / 16))) : nullptr;
					//grid[x][y][z] = new Voxel(Material(Material::Type::DIFF, Color(real(x) / 16, real(y) / 2, real(z) / 16)));
					grid[x][y][z] = getBombermanLevelVoxel(x, y, z);
				}
			}
		}
	}

	VoxelGrid::~VoxelGrid() {
		delete grid;
	}

	Voxel* VoxelGrid::getBombermanLevelVoxel(unsigned x, unsigned y, unsigned z) {
		const unsigned SIZE = 13 - 1;
		if (y == 0) {
			return new Voxel(this, Material(Material::Type::DIFF, Color(real(0.2), real(0.5), real(0))));
		}
		if (x == 0 || x == SIZE || z == 0 || z == SIZE || (x & 1) + (z & 1) == 0) {
			return new Voxel(this, Material(Material::Type::DIFF, Color(real(0.4), real(0.4), real(0.4))));
		}
		if (RNG::instance.next() < 0.5 && z + x > 4 && SIZE - z + x > 4 && SIZE - x + z > 4 && 2 * SIZE - x - z > 4) {
			return new Voxel(this, Material(Material::Type::DIFF, Color(real(0.6), real(0.6), real(0.6))));
		}
		return nullptr;
	}

	bool VoxelGrid::hit(
		HitRecord<real>& hitRecord,
		Ray<real>& ray,
		const real tMin,
		const real tMax,
		const SceneObject* ignore) const
	{
		// Move a little bit in the ray direction, to avoid floating point aliasing
		const Vector3r origRayPos(ray.position);
		ray.movePosition(tMin);

		Vector3i voxelCoord(
			int(ray.position.x),
			int(ray.position.y),
			int(ray.position.z));

		if (voxelCoord.x == -1 || voxelCoord.x == dimensions.x ||
			voxelCoord.y == -1 || voxelCoord.y == dimensions.y ||
			voxelCoord.z == -1 || voxelCoord.z == dimensions.z) { return false; }

		// Move ray position back... TODO: more elegant solution
		ray.position.set(origRayPos);

		Vector3r tGridMax(
			(real(voxelCoord.x + ray.dirIsPos.x) - ray.position.x) * ray.invDirection.x,
			(real(voxelCoord.y + ray.dirIsPos.y) - ray.position.y) * ray.invDirection.y,
			(real(voxelCoord.z + ray.dirIsPos.z) - ray.position.z) * ray.invDirection.z);
		const Vector3r tDelta(
			ray.direction.x == 0 ? INFINITY : ray.dirSign.x * ray.invDirection.x,
			ray.direction.y == 0 ? INFINITY : ray.dirSign.y * ray.invDirection.y,
			ray.direction.z == 0 ? INFINITY : ray.dirSign.z * ray.invDirection.z);

		// Start traversing the grid
		auto minTMaxIndex = -1;
		Voxel* voxel;
		while ((voxel = grid[voxelCoord.x][voxelCoord.y][voxelCoord.z]) == nullptr) {
			minTMaxIndex = tGridMax.minValIndex();
			voxelCoord.el[minTMaxIndex] += ray.dirSign.el[minTMaxIndex];
			auto voxelIdx = voxelCoord.el[minTMaxIndex];
			if (voxelIdx == -1 || voxelIdx == dimensions.el[minTMaxIndex]) { return false; }
			tGridMax.el[minTMaxIndex] += tDelta.el[minTMaxIndex];
		}

		// Calculate normal
		auto tInsideBb = minTMaxIndex == -1 ?
			0 : tGridMax.el[minTMaxIndex] - tDelta.el[minTMaxIndex];

		if (tMax < tInsideBb) { return false; }

		ray.movePosition(tInsideBb);

		auto centerToHit = Vector3r(
			ray.position.x - (real(voxelCoord.x) + toVoxelCenter.x),
			ray.position.y - (real(voxelCoord.y) + toVoxelCenter.y),
			ray.position.z - (real(voxelCoord.z) + toVoxelCenter.z));

		auto maxAbsCenterToHitIndex = centerToHit.maxAbsValIndex();

		hitRecord.surfaceOnb.w.el[maxAbsCenterToHitIndex] = sign(centerToHit.el[maxAbsCenterToHitIndex]);
		hitRecord.surfaceOnb.u.el[(maxAbsCenterToHitIndex + 1) % 3] = 1;
		hitRecord.surfaceOnb.v.el[(maxAbsCenterToHitIndex + 2) % 3] = 1;
		hitRecord.object = voxel;

		return true;
	}

}
