#pragma once

#include <SFML/Graphics/Image.hpp>
#include "Scene.h"
#include "Definitions.h"
#include "Ray.h"
#include "HitRecord.h"

namespace rt {

	class RayTracer {

	public:
		RayTracer(Scene& scene, sf::Image& image) : scene(scene), image(image) {}
		void render();

	private:
		Color tracePath(unsigned depth, const SceneObject* object, Ray<real>& ray) const;

		// Traces a single straight trajectory.
		// Expects ray's position to be at the hit point of object's container, and then transformed into parent local space.
		// After calling traceRay and it returns true, the ray position will be the point of intersection
		// with the hit object in global space.
		bool traceRay(HitRecord<real>& hitRecord, Ray<real>& ray, const SceneObject* object) const;

		Scene& scene;
		sf::Image& image;
		sf::Color backgroundColor;

		const Color BLACK = Color(0, 0, 0);
		const unsigned MIN_DEPTH = 0;
		const unsigned MAX_DEPTH = 1;

		const unsigned AA_R = 2;
		const unsigned AA_THETA = 4;
		const real INV_AA_R = 1 / real(AA_R);
		const real INV_AA_THETA = 1 / real(AA_THETA);
		const real INV_AA = 1 / real(AA_R * AA_THETA);

	};

}
