#pragma once

#include <vector>
#include "SceneObject.h"

namespace rt {

	class UnorderedSpace : public SceneObject {

	public:
		UnorderedSpace(
			const SceneObject* parent = nullptr,
			const Vector3r& position = Vector3r(),
			const Material& material = Material()) :
			SceneObject(parent, position, material) {}

		bool hit(
			HitRecord<real>& hitRecord,
			Ray<real>& ray,
			const real tMin,
			const real tMax,
			const SceneObject* ignore = nullptr) const;

		std::vector<SceneObject*> objects;

	};

}