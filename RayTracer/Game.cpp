#include "Game.h"

namespace rt {

	Game::Game(Scene& scene) : scene(scene) {
		liveObjects.push_back(&scene.camera);
	}

	void Game::update(real dt) {
		for (const auto liveObject : liveObjects) {
			liveObject->update(dt);
		}
	}

}

