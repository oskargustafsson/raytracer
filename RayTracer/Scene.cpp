#include "Scene.h"
#include "Sphere.h"
#include "VoxelGrid.h"
#include "AAPlane.h"
#include "AABB.h"

namespace rt {

	Scene::Scene(Camera camera) : camera(camera) {
		auto levelBounds = new AABB(&root, Vector3r(), Vector3r(13, 2, 13));
		auto level = new VoxelGrid(levelBounds, Vector3u(13, 2, 13));
		levelBounds->objects.push_back(level);

		root.objects.push_back(levelBounds);

		//objects[0] = (SceneObject *)new Sphere(Vector3r(0, 0, 0), 1, Material(Material::Type::DIFF, Color(0, 1, 0)));
		root.objects.push_back(
			(SceneObject *)new AAPlane(
				&root,
				Vector3r(0, 50, 0),
				Vector3r::Axis::Y,
				Material(Material::Type::EMIT, Color(1, 0, 1), Color(1, 1, 1))));
		//objects[2] = (SceneObject *)new Sphere(Vector3r(1, 0, 0), 1, Color(255, 0, 0));
		//objects[3] = (SceneObject *)new Sphere(Vector3r(0, 1, 0), 1, Color(0, 255, 0));
		//objects[4] = (SceneObject *)new Sphere(Vector3r(0, 0, 1), 1, Color(0, 0, 255));
	}

}