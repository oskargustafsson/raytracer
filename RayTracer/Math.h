#pragma once

#include <algorithm>

#define PI 3.141592653589793
#define TWO_PI 6.283185307179586

namespace rt {

	template <typename T> T clamp(const T& val, const T& minVal, const T& maxVal) {
		return std::max(std::min(val, maxVal), minVal);
	}

	template <typename T> T sign(T val) {
		return T((T(0) < val) - (val < T(0)));
	}

}