#pragma once

#include "UnorderedSpace.h"

namespace rt {

	class AABB : public UnorderedSpace {

	public:
		AABB(
			const SceneObject* parent,
			const Vector3r& position,
			const Vector3r& size) :
			UnorderedSpace(parent, position),
			maxPosition(position + size) {}

		bool hit(
			HitRecord<real>& hitRecord,
			Ray<real>& ray,
			const real tMin,
			const real tMax,
			const SceneObject* ignore = nullptr) const;

	private:
		Vector3r maxPosition;

	};

}
