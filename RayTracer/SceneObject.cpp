#include "SceneObject.h"

namespace rt {

	bool SceneObject::update(real dt) { return false; }

	Vector3r SceneObject::getGlobalPosition() const {
		return parent == nullptr ? localPosition : parent->getGlobalPosition() + localPosition;
	}

}