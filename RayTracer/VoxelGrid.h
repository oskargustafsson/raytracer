#pragma once

#include "SceneObject.h"
#include "Vector3.h"
#include "Voxel.h"

namespace rt {

	class VoxelGrid : public SceneObject {

	public:
		VoxelGrid(
			const SceneObject* parent,
			const Vector3u& dimensions/*,
			real voxelSize*/);

		~VoxelGrid();

		bool hit(
			HitRecord<real>& hitRecord,
			Ray<real>& ray,
			const real tMin,
			const real tMax,
			const SceneObject* ignore = nullptr) const;

	private:
		Voxel* getBombermanLevelVoxel(unsigned x, unsigned y, unsigned z);

		Voxel**** grid;
		//real voxelSize; Assume all voxels are of size 1
		//real invVoxelSize;
		Vector3u dimensions;
		Vector3r toVoxelCenter;

	};

}
