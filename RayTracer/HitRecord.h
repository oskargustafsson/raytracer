#pragma once

#include <SFML/Graphics/Color.hpp>
#include "Basis3.h"

class SceneObject;

namespace rt {

	template <typename T> struct HitRecord {
		Basis3<T> surfaceOnb;
		const SceneObject* object;
	};

}