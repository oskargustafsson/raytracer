#pragma once

#include "Basis3.h"
#include "Math.h"
#include "RNG.h"

namespace rt {

	template <typename T> class Ray {

	public:
		Ray<T>(const Vector3<T> position = Vector3<T>(0, 0, 0), const Vector3<T> direction = Vector3<T>(0, 0, 0)) :
			position(position),
			direction(direction) {}
		
		void cacheDerivedMembers() {
			invDirection.x = T(1) / direction.x;
			invDirection.y = T(1) / direction.y;
			invDirection.z = T(1) / direction.z;
			dirIsPos.x = direction.x >= 0;
			dirIsPos.y = direction.y >= 0;
			dirIsPos.z = direction.z >= 0;
			dirSign.x = (char)sign(direction.x);
			dirSign.y = (char)sign(direction.y);
			dirSign.z = (char)sign(direction.z);
		}

		void movePosition(T t) {
			position.add(t, direction);
		}

		void setDirectionOnHemisphere(const Basis3r& surfaceOnb, const real u1, const real u2) {
			// Cosine weighted sampling
			const auto r = sqrt(u1);
			const auto theta = real(TWO_PI) * u2;
			const auto x = r * cos(theta);
			const auto y = r * sin(theta);
			direction.set(x, y, sqrt(real(1) - u1));
			direction *= surfaceOnb;
		}

		Vector3<T> position;
		Vector3<T> direction;
		Vector3<T> invDirection;
		Vector3b dirIsPos;
		Vector3c dirSign;

	};

}