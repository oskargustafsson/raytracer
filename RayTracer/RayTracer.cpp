#include "RayTracer.h"
#include "Vector3Functions.h"

namespace rt {

	void RayTracer::render() {
		auto size = image.getSize();
		auto cameraCenter = scene.camera.localPosition;
		auto invAspectRatio = real(size.y) / real(size.x);

		#pragma omp parallel for schedule(dynamic, 1)
		for (int y = 0; y < (int)size.y; ++y) {
			Vector2r lensCoordinate;
			Vector3r pointOnLens;
			Ray<real> ray(cameraCenter);

			for (unsigned x = 0; x < size.x; ++x) {
				lensCoordinate.x = real(2) * (x / real(size.x) - real(0.5));
				lensCoordinate.y = invAspectRatio * real(2) * (y / real(size.y) - real(0.5));

				scene.camera.getLensPoint(pointOnLens, lensCoordinate);
				pointOnLens.normalize();

				ray.position.set(cameraCenter);
				ray.direction.set(pointOnLens);
				ray.cacheDerivedMembers();

				Color color = tracePath(0, &scene.root, ray);

				image.setPixel(x, y, sf::Color(
					clamp(unsigned(255 * color.r), 0u, 255u),
					clamp(unsigned(255 * color.g), 0u, 255u),
					clamp(unsigned(255 * color.b), 0u, 255u)));
			}
		}
	}

	Color RayTracer::tracePath(unsigned depth, const SceneObject* object, Ray<real>& ray) const {
		HitRecord<real> hitRecord;

		if (!traceRay(hitRecord, ray, object)) {
			return BLACK;
		}

		const Material& material = hitRecord.object->material;

		if (material.type == Material::Type::EMIT) {
			return material.emittance;
		}

		if (depth > MAX_DEPTH) {
			return BLACK;
		}

		Color color = material.color;
		Color traceColor;
		Vector3r origPosition(ray.position);

		for (unsigned aaR = 0; aaR < AA_R; ++aaR) {
			for (unsigned aaTheta = 0; aaTheta < AA_THETA; ++aaTheta) {
				auto u1 = (real(aaR) + RNG::instance.next()) * INV_AA_R;
				auto u2 = (real(aaTheta) + RNG::instance.next()) * INV_AA_THETA;

				ray.position.set(origPosition);
				ray.setDirectionOnHemisphere(hitRecord.surfaceOnb, u1, u2);
				ray.cacheDerivedMembers();

				traceColor += tracePath(depth + 1, object, ray);
			}
		}

		traceColor *= INV_AA;
		color.mul(traceColor);

		return color;
	}

	bool RayTracer::traceRay(HitRecord<real>& hitRecord, Ray<real>& ray, const SceneObject* object) const {
		const SceneObject* prevObject = nullptr;

		// Transform the ray into object local space
		if (object->parent != nullptr) {
			ray.position -= object->parent->getGlobalPosition();
		}

		do {
			if (object->hit(hitRecord, ray, real(0.0001), INFINITY, prevObject)) {
				return true;
			}
			prevObject = object;
			object = object->parent;
		} while (object != nullptr);

		return false;
	}

}