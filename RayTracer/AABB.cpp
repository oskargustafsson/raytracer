#include "AABB.h"

namespace rt {

	bool AABB::hit(
		HitRecord<real>& hitRecord,
		Ray<real>& ray,
		const real tMin,
		const real tMax,
		const SceneObject* ignore) const
	{
		auto tHitMin = tMin;
		auto tHitMax = tMax;

		for (auto i = 0; i < 3; ++i) {
			const auto isDirPos = ray.dirIsPos.el[i];
			const auto invDir = ray.invDirection.el[i];
			const auto pos = ray.position.el[i];

			auto tHitOtherMin = invDir * ((isDirPos ? localPosition : maxPosition).el[i] - pos);
			auto tHitOtherMax = invDir * ((isDirPos ? maxPosition: localPosition).el[i] - pos);

			if (tHitOtherMin > tHitMin) { tHitMin = tHitOtherMin; }
			if (tHitOtherMax < tHitMax) { tHitMax = tHitOtherMax; }
			if (tHitMax < tHitMin) { return false; }
		}

		// Move the ray position forward to the point of intersection
		ray.movePosition(tHitMin * tHitMax < 0 ? 0 : tHitMin);

		return UnorderedSpace::hit(hitRecord, ray, tMin, tMax, ignore);
	}

}
